package ru.tsvelovskiy.module01.unit1;

public class LinearEquationCalculator {
    public EquationRoots calculate(Equation equation){


        EquationRoots roots = new EquationRoots();
        int b = equation.getB();
        int c = equation.getC();

        roots.setX1(calculateRoot(b, c));
        return roots;
    }

    private Double calculateRoot(int b, int c) {
        return (- c) / (double)b;
    }
}
