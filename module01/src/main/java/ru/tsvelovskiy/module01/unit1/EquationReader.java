package ru.tsvelovskiy.module01.unit1;

import java.util.Scanner;

public class EquationReader {
    public Equation readQuadEquation(){
        Scanner sc = new Scanner(System.in);
        int a, b, c;
        do {
            try {

                System.out.println("Введите а:");
                a = sc.nextInt();
                System.out.println("Введите b:");
                b = sc.nextInt();
                if (a == 0 && b == 0) {
                    throw new InvalidFormatException("а и b не могут быть равны нулю");
                }
                System.out.println("Введите c:");
                c = sc.nextInt();
                System.out.println("Уравнение: " + a + "*x^2 + " + b + "*x + " + c + " = 0");
                return new Equation(a, b, c);
            } catch (Exception e) {
                System.out.println("Ошибка ввода: " + e.getMessage());
            }
        } while (true);
    }
}
