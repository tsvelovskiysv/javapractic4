package ru.tsvelovskiy.module01.unit1;


public class EquationRoots {
    private Double x1;
    private Double x2;


    public void setX1(Double x1) {
        this.x1 = x1;
    }

    public void setX2(Double x2) {
        this.x2 = x2;
    }

    public void printRoots() {
        if (x1 == null && x2 == null) {
            System.out.println("Корней нет");
        } else if (x2 != null){
            System.out.printf("Корень x1: %.2f%n", x1);
            System.out.printf("Корень x2: %.2f%n", x2);
        } else {
            System.out.printf("Корень x: %.2f%n", x1);
        }
    }
}
