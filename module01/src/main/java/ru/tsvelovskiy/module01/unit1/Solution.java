package ru.tsvelovskiy.module01.unit1;

public class Solution {
    public static void main(String[] args){
        EquationReader equationReader = new EquationReader();
        Equation equation = equationReader.readQuadEquation();
        Calculator calculator = new Calculator();
        EquationRoots equationRoots = calculator.calculateEquation(equation);
        equationRoots.printRoots();
     }
}
