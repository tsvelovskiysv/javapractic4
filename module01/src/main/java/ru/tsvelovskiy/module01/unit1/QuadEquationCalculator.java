package ru.tsvelovskiy.module01.unit1;

public class QuadEquationCalculator {
    public EquationRoots calculate(Equation equation){
        EquationRoots roots = new EquationRoots();
        int a = equation.getA();
        int b = equation.getB();
        int c = equation.getC();
        final int d = calculateD(a, b, c);
        if (d >= 0) {
            roots.setX1(calculateX1(a, b, d));
            if (d > 0) {
                roots.setX2(calculateX2(a, b, d));
            }
        }
        return roots;
    }
    private int calculateD(int a, int b, int c) {
        int d = b * b - 4 * a * c;
        System.out.printf("d: %d%n", d);
        return d;

    }
    private Double calculateX1(int a, int b, int d) {
        return (-b + Math.sqrt(d)) / (2 * a);
    }
    private Double calculateX2(int a, int b, int d) {
        return (-b - Math.sqrt(d)) / (2 * a);
    }
}
