package ru.tsvelovskiy.module01.unit1;


public class Calculator {

    public EquationRoots calculateEquation(Equation equation) {
        if (equation.isQuad()) {
            QuadEquationCalculator quadCalculator = new QuadEquationCalculator();
            return quadCalculator.calculate(equation);
        } else {
            LinearEquationCalculator linearCalculator = new LinearEquationCalculator();
            return linearCalculator.calculate(equation);
        }
    }


}
