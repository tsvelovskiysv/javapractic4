package ru.tsvelovskiy.module01.unit1;

public class InvalidFormatException extends Exception {
    public InvalidFormatException(String message) {
        super(message);
    }
}
