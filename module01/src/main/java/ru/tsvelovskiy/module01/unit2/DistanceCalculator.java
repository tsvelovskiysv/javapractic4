package ru.tsvelovskiy.module01.unit2;
import static java.lang.Math.sqrt;
import static java.lang.Math.pow;

public class DistanceCalculator {
    public double calculateDistance(Point point1, Point point2){
        return sqrt(pow(point2.getX() - point1.getX(), 2) + pow(point2.getY() - point1.getY(), 2));
    }
}
