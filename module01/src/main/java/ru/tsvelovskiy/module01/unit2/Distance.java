package ru.tsvelovskiy.module01.unit2;


public class Distance {
    public static void main(String[] args) {
        PointReader pointReader = new PointReader();
        DistanceCalculator distanceCalculator = new DistanceCalculator();

        Point point1 = pointReader.read(1);
        Point point2 = pointReader.read(2);
        System.out.println("Расстояние между точками: " + distanceCalculator.calculateDistance(point1, point2));
    }
}
