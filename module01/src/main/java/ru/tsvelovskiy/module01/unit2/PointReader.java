package ru.tsvelovskiy.module01.unit2;

import java.util.Scanner;

public class PointReader {
    public Point read(int number) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите координаты " + number + "точки:");
        System.out.println("Введите x: ");
        int x = sc.nextInt();
        System.out.println("Введите y: ");
        int y = sc.nextInt();
        return new Point(x, y);
    }
}
