package ru.tsvelovskiy.module01.unit3;

import java.util.Objects;

public class Case{
    private Banknote banknote;
    private int count;

    public Case(Banknote banknote, int count) {
        this.banknote = banknote;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public int getValue() {
        return banknote.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Case aCase = (Case) o;
        return banknote == aCase.banknote;
    }

    @Override
    public int hashCode() {
        return Objects.hash(banknote);
    }

    @Override
    public String toString() {
        return banknote.getValue() + " рублей: " + count + " шт.";
    }

    public int countMoney() {
        return banknote.getValue() * count;
    }

}
