package ru.tsvelovskiy.module01.unit3;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public class RequestGetter {

    private List<Case> casesForOutput;
    private List<Case> cases;

    public List<Case> get(int cashRequest, List<Case> cases) {
        casesForOutput = new ArrayList<>();
        this.cases = cases;
        fillCasesForOutput(cashRequest, cases.size() - 1);
        return casesForOutput;
    }

    /**
     * Определяет, можно ли выдать запрос имеющимися купюрами
     */
    private boolean fillCasesForOutput(int cashRequest, int numberOfValuta) {

        if (numberOfValuta < 0) {
            return false;
        }
        int oldCashRequest = cashRequest;
        Case aCase = cases.get(numberOfValuta);
        int wholeRemainder = cashRequest / aCase.getValue();
        int availableBanknoteCount = aCase.getCount();
        int howMuchWeCanGive = min(wholeRemainder, availableBanknoteCount);

        while (howMuchWeCanGive >= 0) {
            cashRequest -= aCase.getValue() * howMuchWeCanGive;
            if (cashRequest == 0 || fillCasesForOutput(cashRequest, numberOfValuta - 1)) {
                casesForOutput.add(new Case(aCase.getBanknote(), howMuchWeCanGive));
                return true;
            } else {
                howMuchWeCanGive--;
                cashRequest = oldCashRequest;
            }
        }
        return false;
    }
}
