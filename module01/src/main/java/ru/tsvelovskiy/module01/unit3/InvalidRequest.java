package ru.tsvelovskiy.module01.unit3;

public class InvalidRequest extends Exception {
    public InvalidRequest(String message){
        super(message);
    }
}
