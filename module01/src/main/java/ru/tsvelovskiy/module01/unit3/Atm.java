package ru.tsvelovskiy.module01.unit3;

import java.util.*;

public class Atm {
    private RequestChecker requestChecker = new RequestChecker();
    private RequestGetter requestGetter = new RequestGetter();
    private List<Case> cases = new ArrayList<>();

    public Atm(int[] quantityOfNotes) {
        for (Banknote banknote : Banknote.values()) {
            cases.add(new Case(banknote, quantityOfNotes[banknote.ordinal()]));
        }
    }

    public void printAtmState() {
        System.out.println("Всего денег в банкомате: " + getTotalSum());
        System.out.printf("%nСодержимое банкомата:%n");
        for (Case cs : cases) {
            System.out.println(cs);
        }
    }

    public void checkRequestAndWithdrawal (int request) {
        try {
            requestChecker.check(request, cases, getTotalSum());
        } catch (InvalidRequest e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
        List<Case> listCasesForOutput = requestGetter.get(request, cases);
        withdrawMoney(listCasesForOutput);
    }

    private void withdrawMoney(List<Case> listCasesForOutput) {
        System.out.printf("%nВыдача денег...%n");
        int difference = cases.size() - listCasesForOutput.size();

        for (int i = listCasesForOutput.size() - 1; i >= 0; i--) {
            Case outCase = listCasesForOutput.get(i);
            Case atmCase = this.cases.get(i + difference);
            atmCase.setCount(atmCase.getCount() - outCase.getCount());
            System.out.println(outCase.getValue() + " рублей: "
                    + outCase.getCount() + " шт.");
        }
        System.out.println("Успешно");
        listCasesForOutput.clear();
    }

    private int getTotalSum() {
        int allMoney = 0;
        for (Case cs : cases) {
            allMoney += cs.countMoney();
        }
        return allMoney;
    }
}
