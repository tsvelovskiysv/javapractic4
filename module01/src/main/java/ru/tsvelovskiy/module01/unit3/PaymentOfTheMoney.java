package ru.tsvelovskiy.module01.unit3;


import java.util.Scanner;

public class PaymentOfTheMoney {
    public static void main(String[] args) {
        int[] quantityOfNotes = {0, 0, 0, 0, 3, 3};
        Atm atm = new Atm(quantityOfNotes);

        //Временный блок для демонстрации работы
        atm.printAtmState();

        System.out.println("Введите сколько хотите получить денег: ");
        Scanner sc = new Scanner(System.in);
        int request = sc.nextInt();
        atm.checkRequestAndWithdrawal(request);

        System.out.printf("%nОсталось в банкомате: %n");
        atm.printAtmState();
    }
}
