package ru.tsvelovskiy.module01.unit3;

import java.util.List;

public class RequestChecker {

    public void check(int request, List<Case> cases, int allMoney) throws InvalidRequest {
        if (request > allMoney) {
            throw new InvalidRequest("В банкомате не хватает средств для выдачи");
        }

        if (!isMultipleToSmallerFaceValue(request, cases)) {
            throw new InvalidRequest("Запрос содержит номиналы купюр, которых нет в банкомате");
        }
    }


    private boolean isMultipleToSmallerFaceValue(int cashOffer, List<Case> cases) throws InvalidRequest{
        return cashOffer % getSmallerValue(cases) == 0;
    }

    private int getSmallerValue(List<Case> cases) throws InvalidRequest{
        for (Case aCase : cases) {
            if (aCase.getValue() > 0) {
                return aCase.getValue();
            }
        }
        throw new InvalidRequest("Банкомат пуст");
    }

}
