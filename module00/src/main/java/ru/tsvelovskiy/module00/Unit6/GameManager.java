package ru.tsvelovskiy.module00.Unit6;

import java.util.Random;
import java.util.Scanner;

public class GameManager {

    private GameField gameField;

    public void createGame(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размер кучи: ");
        int bunch = sc.nextInt();
        System.out.print("Введите размер шага: ");
        int step = sc.nextInt();
        gameField = new GameField(bunch, step);
    }

    public void doStep(){
        String player = gameField.getCurrentPlayer();
        int step = ("Игрок".equals(player)) ? calculatePlayerStepSize() : calculateComputerStepSize();
        gameField.setBunch(gameField.getBunch() - step);
        System.out.printf("%s отнимает %d из кучи. Осталось в куче: %d%n", player, step, gameField.getBunch());
        if (isGameOver()) {
            System.out.printf("%nПобедил %s", player);
        }
        gameField.changePlayer();
    }

    public void firstMoveRight(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите кто ходит первый. 0 - игрок, 1 - компьютер: ");
        int firstPlayerNumber = sc.nextInt();
        gameField.setCurrentPlayer(firstPlayerNumber);
    }

    public boolean isGameOver() {
        return gameField.getBunch() < 1;
    }


    private boolean checkStep(int step){
        if (step > gameField.getStep() || step < 1) {
            System.out.print("Вы играете не по правилам. Вы хотите забрать значение вне диапазона шага!\n");
            return false;
        }
        return true;
    }

    private int calculateComputerStepSize() {
        Random random = new Random();
        if ((gameField.getBunch()) % (gameField.getStep() + 1) != 0) {
            return (gameField.getBunch()) % (gameField.getStep() + 1);
        } else {
            return random.nextInt(gameField.getStep()) + 1;
        }
    }

    private int calculatePlayerStepSize() {
        Scanner sc = new Scanner(System.in);
        int playerStep;
        do {
            System.out.print("Введите сколько забрать: ");
            playerStep = sc.nextInt();
        } while (!checkStep(playerStep));
        return playerStep;
    }

}

