package ru.tsvelovskiy.module00.Unit6;

class Game {

    public static void main(String[] args) {
        GameManager gameManager = new GameManager();
        gameManager.createGame();
        gameManager.firstMoveRight();

        while (!gameManager.isGameOver()) {
            gameManager.doStep();
        }
    }
}
