package ru.tsvelovskiy.module00.Unit6;

class GameField {
    private int bunch;
    private int step;
    private String currentPlayer;
    String[] players = {"Игрок", "Компьютер"};

    public GameField(int bunch, int step){
        this.bunch = bunch;
        this.step = step;
    }

    public int getBunch() {
        return bunch;
    }

    public void setBunch(int bunch) {
        this.bunch = bunch;
    }

    public void changePlayer() {
        switch(getCurrentPlayer()) {
            case "Игрок":
                setCurrentPlayer(1);
                break;
            case "Компьютер":
                setCurrentPlayer(0);
        }
    }

    public int getStep() {
        return step;
    }

    public String getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayerNumber) {
        this.currentPlayer = players[currentPlayerNumber];
    }
}
