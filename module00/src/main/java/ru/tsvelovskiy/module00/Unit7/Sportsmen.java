package ru.tsvelovskiy.module00.Unit7;

public class Sportsmen {
    public static void main(String[] args) {
        int day = 1;
        double kmPerDay = 10;
        double summKm = 10;
        boolean check20KmPerDay = false;
        boolean check100km = false;

        do {
            kmPerDay *= 1.1;
            summKm += kmPerDay;
            day++;
            if (day == 10) {
                System.out.printf("Бегун пробежал за 10 дней %.2f километров %n", summKm);
            }
            if (!check20KmPerDay && kmPerDay > 20) {
                System.out.printf("Бегун пробежал 20 километров на %d день %n", day);
                check20KmPerDay = true;
            }
            if (!check100km && summKm > 100) {
                System.out.printf("Бегун пробежал 100 километров за %d дней %n", day);
                check100km = true;
            }
        } while (day <= 10 || kmPerDay <= 20 || summKm <= 100);
     }
}
