package ru.tsvelovskiy.module00.Unit5;

import java.util.Scanner;

public class Rub {

    public static void main(String[] args) {
        System.out.print("Введите количество рублей от 0 до 1000: ");
        Scanner sc = new Scanner(System.in);
        int rub = sc.nextInt();
        int rubLastOne = rub % 10;
        int rubLastTwo = rub % 100;

        String currency = "";

        if (rubLastTwo >= 11 && rubLastTwo <= 14){
            currency = "рублей";
        } else if (rubLastOne == 1) {
            currency = "рубль";
        } else if (rubLastOne <= 4) {
            currency = "рубля";
        } else {
            currency = "рублей";
        }
        System.out.printf("%d %s",rub,currency);
        sc.close();
    }

}
