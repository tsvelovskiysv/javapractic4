package ru.tsvelovskiy.module00.unit2;

import java.util.Scanner;

public class DateInSeconds {

    public static void main(String[] args) {
        Time time = readTimeAndScanFormat();
        System.out.printf("Время в секундах: %d%n", countSeconds(time));
    }

    private static int countSeconds(Time time) {
        return time.getHour() * 3600 + time.getMinute() * 60 + time.getSecond();
    }

    private static Time readTimeAndScanFormat() {
        Scanner sc = new Scanner(System.in);
        Time time = new Time();

        while (true) {
            System.out.print("Введите время в формате чч:мм:сс : ");
            try {
                String timeStr = sc.nextLine();
                checkTimeFormat(timeStr);

                String[] hhMmSs = timeStr.split(":", 3);
                time.setHour(checkAndGetTimeUnitCount(hhMmSs[0],23));
                time.setMinute(checkAndGetTimeUnitCount(hhMmSs[1],59));
                time.setSecond(checkAndGetTimeUnitCount(hhMmSs[2],59));
                break;

            } catch (InvalidFormatException ex) {
                System.out.println("Введена не валидная строка: "+ ex.getMessage());
            }
        }
        sc.close();
        return time;
    }

    private static boolean checkTimeFormat(String str) throws InvalidFormatException{
        if ((str.length() != 8) || (str.charAt(2) != ':') || (str.charAt(5) != ':')) {
            throw new InvalidFormatException("Ошибка формата");
        } else {
            return true;
        }
    }

    private static int checkAndGetTimeUnitCount(String hourStr, int maxTimeUnitCount) throws InvalidFormatException{
        try {
            int hour = Integer.parseInt(hourStr);
            if (hour < 0 || hour >= maxTimeUnitCount) {
                throw new InvalidFormatException("Время за пределами допустимых значений");
            }
            return hour;
        } catch (NumberFormatException e) {
            throw new InvalidFormatException("Введены не числа");
        }
    }

}