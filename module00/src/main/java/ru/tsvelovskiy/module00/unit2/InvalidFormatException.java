package ru.tsvelovskiy.module00.unit2;

public class InvalidFormatException extends Exception {
    public InvalidFormatException(String message){
        super(message);
    }

    public InvalidFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
