package ru.tsvelovskiy.module00.unit1;

import java.util.Scanner;

public class NumberReverse {
    public static void main (String args[]){
        int numberInt = scanNumberFormat();
        int converselyNumber = 0;
        while (Math.abs(numberInt) > 0)  {
            converselyNumber = converselyNumber * 10 + numberInt % 10;
            numberInt /= 10;
        }
        System.out.printf("Число наоборот: %d", converselyNumber);
    }

    private static int scanNumberFormat() {
        Scanner sc = new Scanner(System.in);
        boolean isFormatRight = false;
        int numberString = 0;
        System.out.print("Введите целое число: ");
        do {
            try {
                numberString = sc.nextInt();
                isFormatRight = true;
            } catch (NumberFormatException ex) {
                System.out.println("Ошибка, введено не целое число. Введите число повторно: ");
            }
        } while (!isFormatRight);
        sc.close();
    return numberString;
    }
}
