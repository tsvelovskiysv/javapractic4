package ru.tsvelovskiy.module00.unit4;

class Date {
    private int day;
    private int month;
    private int year;

    Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    int getDay() {
        return day;
    }

    int getMonth() {
        return month;
    }

    int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return String.format("%02d.%02d.%d", day, month, year);
    }
}
