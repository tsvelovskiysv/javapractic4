package ru.tsvelovskiy.module00.unit4;

import ru.tsvelovskiy.module00.unit2.InvalidFormatException;

import java.util.Scanner;

public class DateReader {

    private final DateChecker checker;
    private final DateMapper mapper;

    public DateReader(DateChecker checker, DateMapper mapper) {
        this.checker = checker;
        this.mapper = mapper;
    }

    public Date readDate() {
        Scanner sc = new Scanner(System.in);
        String dateStr;
        do {
            try {
                System.out.print("Введите дату в формате дд.мм.гггг: ");
                dateStr = sc.nextLine();

                //1. Проверить формат строки
                checker.checkStrDate(dateStr);
                //2. Преобразовать строку в объект Date
                Date date = mapper.createFromString(dateStr);
                //3. Провалидировать объект Date
                checker.checkDate(date);
                return date;
            } catch (InvalidFormatException e) {
                System.out.println("Ошибка ввода: " + e.getMessage());
            }
        } while (true);
    }

}
