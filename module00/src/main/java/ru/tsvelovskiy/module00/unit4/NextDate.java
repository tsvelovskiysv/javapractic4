package ru.tsvelovskiy.module00.unit4;


public class NextDate {

    public static void main(String[] args) {
        DateChecker checker = new DateChecker();
        DateMapper mapper = new DateMapper();
        DateReader reader = new DateReader(checker, mapper);
        Date date = reader.readDate();

        int dayNumber = mapper.conversionToDayNumber(date);
        Date nextDate = mapper.conversionToDate(dayNumber + 1, date.getYear());

        System.out.printf("Следующая дата: ", nextDate.toString());
    }
}

