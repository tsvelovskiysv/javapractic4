package ru.tsvelovskiy.module00.unit4;

import ru.tsvelovskiy.module00.unit2.InvalidFormatException;

public class DateChecker {

    private static final int MAX_STR_DATE_LENGTH = 10;
    private static final char STR_DATE_SEPARATOR = '.';


    public void checkStrDate(String dateStr) throws InvalidFormatException {
        if ((dateStr.length() != MAX_STR_DATE_LENGTH) ||
                (dateStr.charAt(2) != STR_DATE_SEPARATOR) || (dateStr.charAt(5) != STR_DATE_SEPARATOR)) {
            throw new InvalidFormatException("Строка не соответствует формату дд.мм.гггг");
        }
    }


    public void checkDate(Date date) throws InvalidFormatException{
        checkYear(date);
        checkMonth(date);
        checkDay(date);
    }

    private void checkYear(Date date) throws InvalidFormatException{
        if (date.getYear() <= 0) {
            throw new InvalidFormatException("Значение года должно быть больше нуля");
        }
    }

    private void checkMonth(Date date) throws  InvalidFormatException{
        int month = date.getMonth();
        if (month <= 0 || month > 12) {
            throw new InvalidFormatException("Значение месяца должно быть в диапазоне 1..12");
        }
    }

    private void checkDay(Date date) throws InvalidFormatException {
        int[] monthArray = Utils.createArrayOfYear(date.getYear());
        int day = date.getDay();
        if (day <= 0 || day > monthArray[date.getMonth() - 1]) {
            throw new InvalidFormatException("Не верное число дней в месяце");
        }
    }




}
