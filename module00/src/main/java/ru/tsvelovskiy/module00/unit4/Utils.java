package ru.tsvelovskiy.module00.unit4;

public class Utils {

    private static final char YEAR_OF_REFORM = 1582;

    private Utils() {
    }

    public static int[] createArrayOfYear(int year) {
        int[] yearArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (isYearLeap(year)){
            yearArray[2] = 29;
        }
        return yearArray;
    }

    public static boolean isYearLeap(int year) {
        if (isQuadrupleYear(year)) {
            return year <= YEAR_OF_REFORM || isLeapAfterReform(year);
        } else {
            return false;
        }
    }

    private static boolean isQuadrupleYear(int year) {
        return year >= 8 && year % 4 == 0;
    }

    private static boolean isLeapAfterReform(int year) {
        return (year % 100 != 0) || year % 400 == 0;
    }
}
