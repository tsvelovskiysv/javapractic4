package ru.tsvelovskiy.module00.unit4;

import ru.tsvelovskiy.module00.unit2.InvalidFormatException;

public class DateMapper {

    private static final int LEAP_YEAR_DAYS = 366;
    private static final int NOT_LEAP_YEAR_DAYS = 365;

    public Date createFromString(String dateStr) throws InvalidFormatException {
        String[] dateInfo = dateStr.split("\\.");
        try {
            int day = Integer.parseInt(dateInfo[0]);
            int month = Integer.parseInt(dateInfo[1]);
            int year = Integer.parseInt(dateInfo[2]);
            return new Date(day, month, year);
        } catch (NumberFormatException e) {
            throw new InvalidFormatException("Дата введена не числами", e);
        }
    }

    public int conversionToDayNumber(Date date) {
        int[] yearArray = Utils.createArrayOfYear(date.getYear());
        int dayNumber = 0;
        int month = date.getMonth();
        for (int i = 1; i <= month; i++) {
            dayNumber += (i == month) ? date.getDay() : yearArray[i-1];
        }
        return dayNumber;
    }

    Date conversionToDate(int dayNumber, int year){
        int daysInYear = (Utils.isYearLeap(year)) ? LEAP_YEAR_DAYS : NOT_LEAP_YEAR_DAYS;
        int[] yearArray = Utils.createArrayOfYear(year);
        int counter = 0;
        System.out.println(daysInYear);
        if (dayNumber == daysInYear + 1) {
            return new Date(1, 1, ++year);
        }

        while (dayNumber - yearArray[counter] > 0) {
            dayNumber = dayNumber - yearArray[counter];
            counter++;
        }

        return new Date(dayNumber, counter + 1, year);
    }
}
