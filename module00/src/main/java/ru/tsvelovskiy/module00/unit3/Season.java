package ru.tsvelovskiy.module00.unit3;

import ru.tsvelovskiy.module00.unit2.InvalidFormatException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

class Season {
    private static final String TEXT = "Этот месяц соответствует сезону: %s";
    enum Seasons {WINTER, AUTUMN, SUMMER, SPRING}

    public static void main(String[] args) {

        int month = scan();

        if (Arrays.asList(12, 1, 2).contains(month)) {
            System.out.printf(TEXT, Seasons.WINTER);
        } else if (Arrays.asList(3, 4, 5).contains(month)) {
            System.out.printf(TEXT, Seasons.SPRING);
        } else if (Arrays.asList(6, 7, 8).contains(month)) {
            System.out.printf(TEXT, Seasons.SUMMER);
        } else if (Arrays.asList(9, 10, 11).contains(month)) {
            System.out.printf(TEXT, Seasons.AUTUMN);
        }
    }


    private static int scan() {
        Scanner sc = new Scanner(System.in);
        int monthScan=0;
        System.out.print("Введите номер месяца: ");
        do {
            try {
                monthScan = sc.nextInt();
                monthCheck(monthScan);
                break;
            } catch (InputMismatchException ex) {
                System.out.println("Ошибка, введено не число. Введите число повторно: ");
            } catch (InvalidFormatException e) {
                System.out.println(e.getMessage());
            }
        } while (true);
        sc.close();
        return monthScan;
    }

    static void monthCheck (int month) throws InvalidFormatException {
        if (month < 0 || month > 12) {
            throw new InvalidFormatException("Месяц вне диапазона 1..12. Введите число повторно:");
        }
    }
}
